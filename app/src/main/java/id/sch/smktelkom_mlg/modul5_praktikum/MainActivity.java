package id.sch.smktelkom_mlg.modul5_praktikum;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btnmaps, btnpicture, btntoast, btnalarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnmaps = findViewById(R.id.btnmaps);
        btnpicture = findViewById(R.id.btnpicture);
        btntoast = findViewById(R.id.btntoast);
        btnalarm = findViewById(R.id.btnalarm);

        btnalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, alarm.class);
                startActivity(i);

            }
        });

        btntoast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, toast.class);
                startActivity(i);
            }
        });

        btnmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, maps.class);
                startActivity(i);
            }
        });

        btnpicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, picture.class);
                startActivity(i);
            }
        });
    }
}
